import React from 'react';
import {Button} from 'react-native'
import {ContainerBottom, Menu, Item} from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import IconCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { useLinkTo } from '@react-navigation/native';


export default function BottomBlock() {

  const linkTo = useLinkTo();

  return (
    <ContainerBottom>
      <Menu>
        <Item>
          <Icon name="home-outline" size={20} color="#FFF" />
        </Item>
        <Item onPress={() => linkTo('/Home')}>Home</Item>
      </Menu>
      <Menu>
        <Item>
          <Icon name="search" size={20} color="#FFF" />
        </Item>
        <Item onPress={() => linkTo('/Search')}>Search</Item>
      </Menu>
      <Menu>
        <Item>
          <IconMaterial name="queue-play-next" size={20} color="#FFF" />
        </Item>
        <Item onPress={() => linkTo('/Soon')}>Soon</Item>
      </Menu>
      <Menu>
        <Item>
          <IconCommunityIcons
            name="vector-arrange-above"
            size={20}
            color="#FFF"
          />
        </Item>
        <Item onPress={() => linkTo('/About')}>About</Item>
      </Menu>
    </ContainerBottom>
  );
}
