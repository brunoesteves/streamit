import styled from 'styled-components/native';

export const ContainerBottom = styled.View`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  flex-direction: row;
  height: 100px;
  width: 100%;
  background-color: black;
  position: absolute;
  bottom: 00px;
`;

export const Menu = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  font-size: 20px;
  text-transform: uppercase;
`;

export const Item = styled.Text`
  color: white;
  font-size: 20px;
  text-transform: uppercase;
`;
