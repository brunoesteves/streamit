import React, {useState} from 'react';
import {SafeAreaView, ScrollView} from 'react-native';
import TopBlock from './../TopBlock/TopBlock';
import BottomBlock from './../BottomBlock/BottomBlock';
import {Content} from './styles.js';

export default function Layout(props) {
  return props.layout ? (
    <SafeAreaView>
      <Content>
        <ScrollView style={{marginBottom: 0, marginTop: 0}}>
          {props.children}
        </ScrollView>
      </Content>
    </SafeAreaView>
  ) : (
    <SafeAreaView>
      <Content>
        <ScrollView style={{marginBottom: 100, marginTop: 100}}>
          {props.children}
        </ScrollView>
      </Content>
      <TopBlock />
      <BottomBlock />
    </SafeAreaView>
  );
}
