import React, {useState} from 'react';
import {View, Image, ScrollView, Dimensions, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Octicons';
import {Container, DotsArea, Dots} from './styles';

const {width} = Dimensions.get('window');
const height = width * 0.6;

export default function Slider() {
  const [images, setImages] = useState([
    require('`/../../assets/img/provaFinalSlider.png'),
    require('`/../../assets/img/hackersslider.jpeg'),
    require('`/../../assets/img/matrixSlider.jpg'),
  ]);

  const [name, setName] = useState(['provaFinal', 'hackers', 'matrix']);

  const [active, setActive] = useState(true);

  const change = ({nativeEvent}) => {
    const slide = Math.ceil(
      nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width,
    );
    if (slide !== active) {
      setActive(slide);
    }
  };

  return (
    <Container style={{width, height}}>
      <ScrollView
        onScroll={change}
        pagingEnabled
        horizontal
        showsHorizontalScrollIndicator={false}
        style={{width, height}}>
        {images.map((image, i) => {
          return (
            <View key={i}>
              <Image
                source={image}
                style={{width, height, resizeMode: 'stretch'}}
              />
              <View
                style={{position: 'absolute', bottom: 30, alignSelf: 'center'}}>
                <Text
                  style={{
                    color: 'white',
                    fontSize: 30,
                    textTransform: 'uppercase',
                  }}>
                  {name[i]}
                </Text>
              </View>
            </View>
          );
        })}
      </ScrollView>
      <DotsArea>
        {images.map((res, i) => {
          return (
            <Dots key={i}>
              <Icon
                name="primitive-dot"
                size={25}
                color={i === active ? '#FFF' : 'black'}
              />
            </Dots>
          );
        })}
      </DotsArea>
    </Container>
  );
}
