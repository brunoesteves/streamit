import styled from 'styled-components/native';

export const Container = styled.View`
  margin-top: 0px;
  margin-bottom: 20px;
`;

export const DotsArea = styled.View`
  flex-direction: row;
  position: absolute;
  bottom: 0;
  align-self: center;
`;

export const Dots = styled.Text`
  margin-right: 10px;
`;
