import styled from 'styled-components/native';

export const ItemArea = styled.View`
  height: 200px;
  margin-top: 20px;
  background-color: red;
  margin: 5px;
`;

export const ResultArea = styled.View`
  margin-top: 10px;
  flex-direction: row;
  flex-wrap: wrap;
`;
