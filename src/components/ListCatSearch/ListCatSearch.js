import React, {useState} from 'react';
import {View, Image, ScrollView, Dimensions, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Octicons';
import {ItemArea, ResultArea} from './style';

const {width} = Dimensions.get('window');

export default function ListHome() {
  const [images, setImages] = useState([
    require('`/../../assets/img/escaperoom.jpg'),
    require('`/../../assets/img/tombRaider.jpg'),
    require('`/../../assets/img/matrix.jpg'),
    require('`/../../assets/img/aorigem.jpg'),
    require('`/../../assets/img/avengers.jpg'),
    require('`/../../assets/img/comportamentosuspeito.jpg'),
    require('`/../../assets/img/escaperoom.jpg'),
    require('`/../../assets/img/hackers.jpg'),
    require('`/../../assets/img/lucy.jpg'),
    require('`/../../assets/img/mentebrilhante.jpg'),
    require('`/../../assets/img/oldBoy.jpg'),
    require('`/../../assets/img/oldGuard.jpg'),
    require('`/../../assets/img/provaFinal.jpeg'),
  ]);

  const [category, setCategory] = useState([
    'Aventura',
    'Biográfico',
    'Comédia',
    'Drama',
    'Histórico',
    'Ficção científica',
    'Terror',
    'Suspense',
  ]);

  return (
    <ScrollView>
      <ResultArea>
        {images.map((img, i) => {
          return (
            <ItemArea style={{width: width / 3.3}}>
              <Image key={i} 
                source={img}
                style={{
                  flex: 1,
                  width: null,
                  height: null,
                  resizeMode: 'cover',
                }}
              />
            </ItemArea>
          );
        })}
      </ResultArea>
    </ScrollView>
  );
}
