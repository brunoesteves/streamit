import React, {useState} from 'react';
import {ContainerTop, Item} from './styles';
import {Image} from 'react-native';

import {useLinkTo} from '@react-navigation/native';

export default function TopBlock() {
  const linkTo = useLinkTo();

  return (
    <ContainerTop>
      <Image
        style={{width: 50, height: 50}}
        source={require('../../assets/img//logo.png')}
      />
      <Item onPress={() => linkTo('/Home')}>Home</Item>
      <Item onPress={() => linkTo('/movies')}>Movies</Item>
      <Item onPress={() => linkTo('/clips')}>Clips</Item>
    </ContainerTop>
  );
}
