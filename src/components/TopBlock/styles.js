import styled from 'styled-components/native';

export const ContainerTop = styled.View`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  flex-direction: row;
  height: 100px;
  width: 100%;
  background-color: black;
  position: absolute;
  top: 0;
`;

export const Item = styled.Text`
  color: white;
  font-size: 18px;
  text-transform: uppercase;
`;
