import React, {useEffect, useState} from 'react';
import {View, Image, ScrollView, Dimensions, Text} from 'react-native';
import {set} from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/Octicons';
import {Container, CatName, ItemArea} from './style';
import Content from '../../services/Contents';

export default function ListHome() {
  const [images, setImages] = useState([
    require('`/../../assets/img/escaperoom.jpg'),
    require('`/../../assets/img/tombRaider.jpg'),
    require('`/../../assets/img/matrix.jpg'),
    require('`/../../assets/img/aorigem.jpg'),
    require('`/../../assets/img/avengers.jpg'),
    require('`/../../assets/img/comportamentosuspeito.jpg'),
    require('`/../../assets/img/escaperoom.jpg'),
    require('`/../../assets/img/hackers.jpg'),
    require('`/../../assets/img/lucy.jpg'),
    require('`/../../assets/img/mentebrilhante.jpg'),
    require('`/../../assets/img/oldBoy.jpg'),
    require('`/../../assets/img/oldGuard.jpg'),
    require('`/../../assets/img/provaFinal.jpeg'),
  ]);

  const [category, setCategory] = useState([
    'Aventura',
    'Biográfico',
    'Comédia',
    'Drama',
    'Histórico',
    'Ficção científica',
    'Terror',
    'Suspense',
  ]);

  return (
    <View>
      <ScrollView scrollEventThrottle={16}>
        <View style={{flex: 1}}>
          <Container>
            {category.map((cat, i) => {
              return (
                <>
                  <CatName key={i}>{cat}</CatName>
                  <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}>
                    {images.map((img, index) => {
                      return (
                        <ItemArea key={index}>
                          <View style={{flex: 1}}>
                            <Image
                              source={img}
                              style={{
                                flex: 1,
                                width: null,
                                height: null,
                                resizeMode: 'cover',
                              }}
                            />
                          </View>
                        </ItemArea>
                      );
                    })}
                  </ScrollView>
                </>
              );
            })}
          </Container>
        </View>
      </ScrollView>
    </View>
  );
}
