import styled from 'styled-components/native'

export const Container = styled.View`
     height: 2000px;
      margin-top:20px;
`;


export const CatName = styled.Text`
    color: white;
    margin-left: 15px;
    margin-bottom:10px;
    font-size: 20px;
    text-transform: uppercase;
`;

export const ItemArea = styled.View`
    height: 180px;
    width:100px;
    margin-left:15px;
`;