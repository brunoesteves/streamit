import React from 'react';
import {Item} from '../../assets/GlobalStyles';
import {Image, View} from 'react-native';
import {Container} from './styles';
import Layout from '../../components/Layout/Layout';

export default function About() {
  return (
    <Layout>
      <Container>
        <Image source={require('../../assets/img/logo.png')} />
        <Item style={{marginTop: 20}}>
          Esse local vai um texto que fala sobre a empresa
        </Item>
      </Container>
    </Layout>
  );
}
