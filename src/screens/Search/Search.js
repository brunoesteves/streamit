import React from 'react';
import {Text} from 'react-native';
import {SearchInput, SearchText, SearchIcon} from './styles';
import Icon from 'react-native-vector-icons/Ionicons';
import ListCatSearch from './../../components/ListCatSearch/ListCatSearch';
import Layout from '../../components/Layout/Layout';

export default function Search() {
  return (
    <Layout>
      <SearchInput>
        <SearchText />
      </SearchInput>
      <SearchIcon>
        <Text>
          <Icon name="search" size={30} color="#000" />
        </Text>
      </SearchIcon>
      <ListCatSearch />
    </Layout>
  );
}
