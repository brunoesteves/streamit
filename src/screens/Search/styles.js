import styled from 'styled-components/native';

export const SearchInput = styled.TextInput`
    height: 50px;
    color: white;
    background-color: gray;
    border-radius: 5px;
    margin: 0 7px;
    border-width:3px
    border-color: white;
`;

export const SearchText = styled.Text`
  font-size: 20px;
`;
export const SearchIcon = styled.View`
  position: absolute;
  right: 12px;
  bottom: 10px;
`;
