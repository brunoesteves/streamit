import React, {useState} from 'react';
import {View, Image, Dimensions, Text, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import Layout from '../../components/Layout/Layout';
import {ContainerList, Item} from './styles';

const {width, height} = Dimensions.get('window');
const imageHeight = width * 0.6;

export default function Soon() {
  const [images, setImages] = useState([
    require('`/../../assets/img/provaFinalSlider.png'),
    require('`/../../assets/img/hackersslider.jpeg'),
    require('`/../../assets/img/matrixSlider.jpg'),
    require('`/../../assets/img/matrixSlider.jpg'),
  ]);

  const [name, setName] = useState(['provaFinal', 'hackers', 'matrix']);

  return (
    <Layout>
      <ScrollView>
        {images.map((image, i) => {
          return (
            <View key={i}>
              <Image
                source={image}
                style={{
                  width: width,
                  height: imageHeight,
                  resizeMode: 'stretch',
                  marginTop: 20,
                }}
              />
              <ContainerList>
                <Item>{name[i]}</Item>
                <Item>
                  <Icon name="share" size={20} color="#FFF" />
                </Item>
              </ContainerList>
            </View>
          );
        })}
      </ScrollView>
    </Layout>
  );
}
