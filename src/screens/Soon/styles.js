import styled from 'styled-components/native';

export const ContainerList = styled.View`
  margin-top: 20px;
  justify-content: space-around;
  flex-direction: row;
`;

export const Item = styled.Text`
  color: white;
  text-transform: uppercase;
  font-size: 15px;
  margin-top: 10px;
  margin-bottom: 10px;
`;
