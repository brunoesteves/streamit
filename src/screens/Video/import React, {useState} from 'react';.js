import React, {useState} from 'react';
import {Dimensions, View, Button, Text} from 'react-native';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import Orientation from 'react-native-orientation-locker';
import Video from 'react-native-video';
import ScreenOrientation, {
  PORTRAIT,
  LANDSCAPE,
} from 'react-native-orientation-locker/ScreenOrientation';

export default function VideoPlayer() {
  const {width, height} = Dimensions.get('window');
  const videourl = '../../assets/1seg.mp4';

  const [portrait, setPortrait] = useState(true);


  
  return (
    <>
      <ScreenOrientation orientation={portrait ? PORTRAIT : LANDSCAPE} />

      <View>
        <Video
          onBack={() => setPortrait(true)}
          resizeMode="contain"
          source={require('../../assets/1seg.mp4')} // Can be a URL or a local file.
          style={{
            width: portrait? width : width,
            height: portrait? 250: height,
          }}
        />
      </View>
    </>
  );
}
