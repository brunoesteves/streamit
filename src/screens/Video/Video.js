import React, {useEffect, useState, useRef} from 'react';
import {Text, View, Dimensions} from 'react-native';
// import Orientation from 'react-native-orientation';

import VideoPlayer from 'react-native-video-controls';
import Layout from '../../components/Layout/Layout';

const {height, width} = Dimensions.get('window');

export default function Video() {
  const [isFullScreen, setIsFullScreen] = useState(false);
  const vid = useRef(null);
  const videoHeight = height;

  function enterFullscreen() {
    Orientation.lockToLandscape();
    setIsFullScreen(true);
  }

  function exitFullscreen() {
    Orientation.lockToPortrait();
    setIsFullScreen(false);
  }

  function onBack() {
    Orientation.lockToPortrait();
    setIsFullScreen(true);
  }

  return (
    <Layout layout={isFullScreen}>
      <View
        style={{
          width: width,
          height: isFullScreen ? videoHeight / 1.02 : videoHeight / 2,
        }}>
        <VideoPlayer
          ref={vid}
          source={require('../../assets/video3.mp4')}
          toggleResizeModeOnFullscreen={false}
          title="Video"
          seekColor="#ffff22"
          disableBack={true}
          onBack={() => onBack()}
          onEnterFullscreen={() => enterFullscreen()}
          onExitFullscreen={() => exitFullscreen()}
        />
      </View>
    </Layout>
  );
}
