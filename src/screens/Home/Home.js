import React from 'react';
import Slider from '../../components/Slider/Slider.js';
import ListHome from '../../components/ListHome/ListHome';
import Layout from '../../components/Layout/Layout.js';
import { Button } from 'react-native';

export default function Home() {
  return (
    <Layout>
      <Slider />
      <ListHome />
    </Layout>
  );
}
