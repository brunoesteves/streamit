import {base_url} from './config';

const Content = {
  content: () => {
    return fetch(`${base_url}`).then(res => res.json());
  },
};
